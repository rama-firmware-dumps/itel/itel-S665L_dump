#!/bin/bash

cat product/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> product/priv-app/Velvet/Velvet.apk
rm -f product/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat product/priv-app/Messages/Messages.apk.* 2>/dev/null >> product/priv-app/Messages/Messages.apk
rm -f product/priv-app/Messages/Messages.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat system_ext/priv-app/ItelSettings/ItelSettings.apk.* 2>/dev/null >> system_ext/priv-app/ItelSettings/ItelSettings.apk
rm -f system_ext/priv-app/ItelSettings/ItelSettings.apk.* 2>/dev/null
cat system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null >> system_ext/app/TranssionCamera/TranssionCamera.apk
rm -f system_ext/app/TranssionCamera/TranssionCamera.apk.* 2>/dev/null
cat system_ext/app/AiGallery/AiGallery.apk.* 2>/dev/null >> system_ext/app/AiGallery/AiGallery.apk
rm -f system_ext/app/AiGallery/AiGallery.apk.* 2>/dev/null
cat system/system/preloadapp/WPSOffice/WPSOffice.apk.* 2>/dev/null >> system/system/preloadapp/WPSOffice/WPSOffice.apk
rm -f system/system/preloadapp/WPSOffice/WPSOffice.apk.* 2>/dev/null
cat system/system/preloadapp/TikTokzhiliao/TikTokzhiliao.apk.* 2>/dev/null >> system/system/preloadapp/TikTokzhiliao/TikTokzhiliao.apk
rm -f system/system/preloadapp/TikTokzhiliao/TikTokzhiliao.apk.* 2>/dev/null
cat system/system/preloadapp/TikTok/TikTok.apk.* 2>/dev/null >> system/system/preloadapp/TikTok/TikTok.apk
rm -f system/system/preloadapp/TikTok/TikTok.apk.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
